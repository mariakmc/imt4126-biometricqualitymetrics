function computeBrisque()
    imgsdir = "img/CasiaV2/CasiaSelection/";
    control = 1; %Determines what dataset to use 

   % For all subjects
    for i =1:107

        % Determine subject name
        if control ==1
            if (i) <10 
                nr = "00000" + (i); 
            elseif (i) <100
                nr = "0000" + (i); 
            else
                nr = "000" + (i); 
            end
        else
            if (i) <10 
                nr = "0" + (i); 
            else
                nr = "" + (i); 
            end
        end
        
        sub_nr = "0" + nr; 

        % Calculate for each sample
        for j=1:5
            high_path = imgsdir; 
            path = high_path  +  sub_nr + '/' + '00' + (j) + '.jpg'; 
            img = imread(path);
            quality_scores(i,j) = brisque(img);
            
        end
    end

    s = size(quality_scores); 
    tot= s(1)*s(2);
    disp("Mean")
    disp(sum(sum(quality_scores))/tot)
    disp("STD")
    disp(std(quality_scores(:)))

    norm_scores = Normalize_Q(quality_scores);
    disp(size(norm_scores))
    writematrix( norm_scores, "data/brisque/quality_values_aug.csv")

end

% Nomralize the Brisque values 
function q= Normalize_Q(qualityscores) 
    m = 1; 
    v = [0,0,0,0,0,0,0];

    % Sigmoid
    for i=1:107
        for j=1:5
            qualityscores(i,j) = sigmoid(qualityscores(i,j),40.690877506627977,  8.5);
        end
    end

    % Bin vaules
    for i=1:107
        for j=1:5

            if qualityscores(i,j)< 20
                m = 1; 
            elseif qualityscores(i,j)< 40 && qualityscores(i,j)  >= 20
                m = 2; 
            elseif qualityscores(i,j)< 60 && qualityscores(i,j)  >= 40
                m = 3; 
            elseif qualityscores(i,j)< 80 && qualityscores(i,j)  >= 60
                m = 4; 
            else
                m = 5; 
            end

            v(m) = v(m) +1; 
            q(i,j) = m;
        end
    end

    disp(v)
     
end

% Sigmoid
function q = sigmoid(x,a,b)
     q =  (1-(1 / (1 + exp((a-x)/b) )) )*100;
end
# IMT4126-BiometricQualityMetrics

This is the documentation of the code for the project

### Install Third party Libraries
As the script depends on the ArcFace library to compute the scores, this library needs to be installed first. 
The library installation can be found here: https://github.com/deepinsight/insightface/tree/master/python-package  


## Running the opertaions

In theory all 3 operations can be run in the same instance, or in three sepearate runs.  It can be beneficial to perform the operationsin two runs; first compute the baselines, and then compute and plot the metrics. The ``QualityEvaluator`` object is the heart of all computations. The first argument when initiating this class is the instance name. In main.py this is "ArcFaceCasiaV2", although it can be anything. Just note that all filenames that are written will be tied to the instance name, so for instance a QualityEvaluator object used to compute metrics needs to have the same instance name as the QualityEvaluator objec used to plot them. One can use the same object if these operation is done in the same run. 

If you need to use a different name of the metric file than the instance name, on can specify a different filename when plotting; 

 ``QualEval.PlotMetric("DET", "focus",subnr=params[0], sampnr=params[1], metric_name="path/to/metric/focus_values")``

 Using the instance name is however best. 

For details on how to perform these three operations, read the following sections:


### 0. Setting project path

 in main.py, Change the projectpath to the project path as it is on your local machine

`` projectpath = "my/project/path/" `` 

### 1. Computing the baseline. 
One can use the code present in main.py to compute the baseline. In order to compute, one have to first init a ``QualityEvaluator`` object. Then, one needs to init the ArcFace method, and read in the dataset. One can then run the ``computeBaselines()`` method, which will compute the baselines. If one wants to save the baselines to file, one needs to call the ``saveBasline()`` mehtod. To do this main, do the following:  


#### i. Uncomment the following lines in main.py: 



``QualEval = QualityEvaluator("ArcFaceCasiaV2", projectpath, imgsdir, "subjectnr/samplenr")``

``QualEval.ArcFace_init()``

``QualEval.readInDataset(params, projectpath, imgsdir, "ArcFaceCasia", True ) ``

``QualEval.ComputeBaselines()``

``QualEval.SaveBaslines()``

#### ii. Run main.py


### 2. Compute quality metrics

This can be run as long as there are some baselines (.npy file) in data/ . One does not need to have run ``computeBaselines()`` in the same run. In order to compute metrics, one needs to init a ``QualityEvaluator`` object, and run ``Detection_Arcface_init()`` in order to init ArcFace. Then, one needs to call ``ComputeMetric("name", subjectnr, samplenr)``. If the metric is suported, it will be computed. Then, one needs to call the ``SaveMetric()`` if one wants the metric to be read to file. This cane be done using main by doing the following: 

#### i. Uncomment the following lines in main.py: 



``QualEval = QualityEvaluator("ArcFaceCasiaV2", projectpath, imgsdir, "subjectnr/samplenr")``

``QualEval.Detection_Arcface_init()``

``QualEval.ComputeMetric("sharpness", params[0], params[1])``

``QualEval.ComputeMetric("focus", params[0], params[1])``

``QualEval.SaveMetric("sharpness")``

``QualEval.SaveMetric("focus")``

 
``

#### ii. Run main.py

#### iii. Run the script computeBrisque.m. 



### 3. Plot quality metrics


This can be run as long as there are some baselines (.npy file in data/), and some quality scores for the specified metric (.npy file in data/ or .csv file in data/brisque/ ). In order to plot metrics, one needs to init a ``QualityEvaluator`` object and run ``Detection_Arcface_init()`` . Then, one needs to call ``PlotMetric("Method", "Metric", subnr=[subnr], samnr=[sampnr])``. If the metric is suported, it will be plotted. In order to do this in main, do the following: 

#### i. Uncomment the following lines in main.py: 


``QualEval = QualityEvaluator("ArcFaceCasiaV2", projectpath, imgsdir, "subjectnr/samplenr")``

``QualEval.Detection_Arcface_init()``

``QualEval.PlotMetric("EDC", "focus",subnr=params[0], sampnr=params[1])``

``QualEval.PlotMetric("EDC", "sharpness",subnr=params[0], sampnr=params[1])``

``QualEval.PlotMetric("EDC", "brisque",subnr=params[0], sampnr=params[1], metric_name="data/brisque/quality_values_aug")``

``QualEval.PlotMetric("DET", "focus",subnr=params[0], sampnr=params[1])``

``QualEval.PlotMetric("DET", "sharpness",subnr=params[0], sampnr=params[1])``

``QualEval.PlotMetric("DET", "brisque",subnr=params[0], sampnr=params[1], metric_name="data/brisque/quality_values_aug")``

``QualEval.PlotDETCurve(params[0],params[1])``

#### ii. Run main.py


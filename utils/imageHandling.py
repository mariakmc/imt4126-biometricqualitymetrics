import numpy as np
from insightface.data import get_image as ins_get_image


'''
Hold all image information
'''
class img_o:
    def __init__(self, img, face, name ):
        self.img = img
        self.face = face
        self.feat = None
        self.name = name

    
'''
Perform image operations
'''
class Image_Preparator:
    def __init__(self, app) -> None:
        self.app = app
        pass

    '''
    Read in image
    '''
    def read_in(self, path: str): 
        image = ins_get_image(path)
        return  image

    '''
    Segment face images
    '''
    def segment(self, image):       
        face = self.app.get(image)  
        box = face[0].bbox.astype(np.int)
        crop_img = image[box[1]:box[3],  box[0]:box[2]].copy()
        return crop_img
    
    '''
    Segment face square
    '''
    def segment_square(self, image):
        face = self.app.get(image)
        box = face[0].bbox.astype(np.int)
        
        # Determine smallest dimension
        x = box[3] - box[1]
        y = box[2] - box[0]

        diff = abs(x-y)

        if (np.mod(diff,2)!=0):
            diff1 = int(diff/2)  +1 
            diff2 = int(diff/2)
        else:
            diff1 = int(diff/2)  
            diff2 = int(diff/2)

        #Mod smallest dimension
        if x < y: 
            box[3] = box[3] +diff1
            box[1] = box[1] -diff2
        elif y<x:
            box[2] = box[2] + diff1
            box[0] = box[0] - diff2

        return image[box[1]:box[3],  box[0]:box[2]].copy()

    '''
    Compute luiminence (as it is defined in the ISO Standard)
    '''
    def luimanence(self, image):
        lum = np.zeros(shape=(len(image), len(image[0])))
       
        for i in range(len(image)):
            for j in range(len(image[0])):
                convert = False
                t = 0.04045
                npx = [0,0,0]
                # Normalize each pixel
                for k in range(3):
                    nc = image[i][j][k]/255
                    if(nc <= t): convert = True

                    # Perform Gamma Inversion
                    if(convert):
                        if(nc > t): 
                            nc =  (nc + 0.055)/1.0558
                        else: 
                            nc = nc/ 12.92
                            nc = pow(nc, 2.4)
                    npx[k] = nc
                
                # Calculate luiminance
                lum[i][j] = 0.2126*npx[0]+ 0.7152*npx[1] + 0.0722*npx[2]
        return lum

    def ColorConvert(V):
        return -1


'''
Helping Class That reads in images
'''
class Reader: 
    def __init__(self, method="ArcFace") -> None:
        
        if method == "ArcFace": 
            self.readMethod = self.readArcface
        else: 
            self.readMethod = self.readGeneric

    def read(self, imgpath: str, imgname:str, other:bool):
        return self.readMethod(imgpath, imgname, other)

    def setArcFaceVars(self, app, handler):
        self.app = app
        self.handler = handler
        return

    def readArcface(self,imgpath:str,  imgname: str, withFeatures = True):
        image = ins_get_image(imgpath, imgname)

        if withFeatures: face = self.app.get(image)
        else: face = []

        imgob = img_o(image, face, imgname)
        if withFeatures: imgob.feat = self.handler.get(imgob.img, imgob.face[0])

        return imgob

    def readGeneric(self, img: str): 
        raise Exception("Not Implemented Yet")

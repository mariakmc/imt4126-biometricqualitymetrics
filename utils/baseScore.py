class BaseHandler:
    def __init__(self, subject_tot: int, sample_tot: int) -> None:
        self.subject_tot = subject_tot
        self.sample_tot = sample_tot

        pass

    '''
    Takes in the subject number and sample number of two samples, and returns the index
    of the correlating similarity score in the similarity_score list. 
    '''
    def get(self, subject_nr_1: int, saple_nr_1: int, subject_nr_2: int, saple_nr_2: int): 
       
        n = self.subject_tot * self.sample_tot
        a = self.sample_tot

        
        if(subject_nr_1<subject_nr_2 or (subject_nr_1 == subject_nr_2 and saple_nr_1< saple_nr_2)):
            i = subject_nr_1
            j = saple_nr_1
            k = subject_nr_2
            l = saple_nr_2
        else:
            k = subject_nr_1
            l = saple_nr_1
            i = subject_nr_2
            j = saple_nr_2
        
       
        
        v = self.subject_tot - i -1 
        u = self.sample_tot - j

        scnd_element = k*a + l - (i * a + j)

        indx = int((n*(n+1))/2) - int(((v*a+u)*(v*a+u+1))/2) + scnd_element
        return indx
    
    def write(sim_scores: list) -> None:

        return
    
    def read() -> list:
        return []

    

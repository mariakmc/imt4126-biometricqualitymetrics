
import numpy as np
import matplotlib.pyplot as mpl




'''
    Computes the EDC (error versus discard) specifically for quality values.
    This works for discrete quality values where the value is a scalar

    Heavily based on provided DET curve script. 

    Also called ERC (error versus reject)
'''
class EDC: 
    def __init__(self, lowerbound, higherbound, fixed_fmnr_threshold = 0.3, metricname="", style="-") -> None:
        self.lowb = lowerbound
        self.highb = higherbound
        self.threshold = fixed_fmnr_threshold
        self.style = style
        if  metricname != "": self.title = "EDC-Curve for " + metricname
        else: self.title = "EDC-Curve"
        pass

    
    '''
    Calcualte the fnmr
    '''
    def _fmnr(self, sims): 
        sims_sorted_inx = np.argsort(sims, kind='mergesort')
        sims_sorted = sims[sims_sorted_inx]
        length= len(sims_sorted)
        inx = 0
        breaks = False
        while(inx < length and not breaks ): 
            if(sims_sorted[inx] > self.threshold):
                breaks = True
            inx =inx +1
        fnmr = (inx-1)/length
        return fnmr

    '''
    Plot the EDC 
    '''
    def plotEDC(self, gens, qual_gens): 
        
        # Init starting values
        x = []
        y = []
        
        totnumValues = gens.shape[0]
        totRemoved = 0
        curr_gen = []
        curr_quals = []

        curr_gen.append([gens])
        curr_quals.append([qual_gens]) 

        
        x.append(0)
        y.append(self._fmnr(gens))
        indx  = 0

        # Run for all combinations of (u,v)
        for u in range(self.lowb+1, self.highb + 1): 

            ## Calculate for (u, lower_bound)
            round_gens = curr_gen[u-self.lowb-1][0]
            round_quals = curr_quals[u-self.lowb-1][0]

            # Remove values
            r_0, fnmr, q_inx_spliced = self.discardValues(round_gens, round_quals[:, 0], u)
            totRemoved = totRemoved +  r_0
            totRemoved_v_round = totRemoved

            # Set new scores
            new_quals = round_quals[q_inx_spliced]
            new_gens = round_gens[q_inx_spliced]
            curr_gen.append([new_gens])
            curr_quals.append([new_quals])

            # Calculate rejection rate
            r = totRemoved/totnumValues

            # Append
            x.append(r)
            y.append(fnmr)
            print("(",u, ", ", 1, ") -> FNMR: ",fnmr, " || ", len(q_inx_spliced))
            

            for v in range(self.lowb  +1 , self.highb + 1):
                
                # Remove values
                # Calculate r and v on those left
                round_gens = curr_gen[u-self.lowb][v-self.lowb-1]
                round_quals = curr_quals[u-self.lowb][v-self.lowb-1]

                # calculate the rejection rate 
                r_0, fnmr,q_inx_spliced = self.discardValues(round_gens, round_quals[:, 1], v)
                totRemoved_v_round = totRemoved_v_round + r_0
                r = totRemoved_v_round/totnumValues

                # Append
                x.append(r)
                y.append(fnmr)
                #print("(",u, ", ", v, ") -> FNMR: ",fnmr, " || ", len(q_inx_spliced))

               
                new_gens = round_gens[q_inx_spliced]
                new_quals = round_quals[q_inx_spliced]

                curr_gen[u-self.lowb].append(new_gens)
                curr_quals[u-self.lowb].append(new_quals)

 
        # Format x,y
        x = np.array(x)
        y = np.array(y)

        #print(x)
        #print(y)
        #print("\n")

        indx = np.argsort(x, axis=0)
        x_sort = x[indx]
        y_sort = y[indx]
        
        #print(x_sort)
        #print(y_sort)
        
        mpl.plot(x_sort, y_sort, label="label", color=(0.3, 0.3, 0.0), linestyle=self.style, linewidth=1)
        return

    '''
    Removes values based one one quality value
    '''
    def discardValues(self, sims, quals, level): 
        total_scores = len(sims)

        # Sort by quality value
        q_inx = np.argsort(quals, kind='mergesort')
        quals_sorted  = quals[q_inx]
       
        # Remove low quality values 
        inx = 0
        breaks = False
        while(inx < total_scores and not breaks ): 
            if(quals_sorted[inx] >= level):
                breaks = True
            inx =inx +1

        q_inx_spliced = q_inx[inx-1:]

        # Calculate fmnr
        sims_spliced= sims[q_inx_spliced]
        sims_spliced_sorted_inx = np.argsort(sims_spliced, kind='mergesort')

        sims_spliced_sorted = sims_spliced[sims_spliced_sorted_inx]

        # FMNR
        tot_not_rejected = len(q_inx_spliced)
        inx = 0
        breaks = False
        while(inx < len(sims_spliced_sorted) and not breaks ): 
            if(sims_spliced_sorted[inx] > self.threshold):
                breaks = True
            inx =inx +1

        fnmr = (inx-1)/tot_not_rejected

        return total_scores- tot_not_rejected, fnmr,  q_inx_spliced

    '''
    Init figure
    '''
    def setup(self): 
        self.figure = mpl.figure() 
        mpl.xlabel("Percentage Removed")
        mpl.ylabel("FNMR (False Non Match Rate)")
        mpl.title(self.title)

    '''
    Show EDC plot
    '''
    def showEDC(self): 
        # Show curve 
        mpl.show()
        return 


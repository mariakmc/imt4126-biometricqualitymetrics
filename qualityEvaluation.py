import csv
import numpy as np
from utils.baseScore import BaseHandler
from DET.DET import DET
from insightface.app import FaceAnalysis
import insightface
from EDC import EDC
from utils.imageHandling import Reader, img_o, Image_Preparator
import scipy


colour = [ "Kitty", "Disco", "Rabbit","Dog" , "Card", "Mickey", "Mask"]

class QualityEvaluator:
    def __init__(self, instanceName, projectpath, imgsdir, folderstructure) -> None:
        self.instaceName =instanceName
        self.setFolderStructure = folderstructure

        # TODO: Consider class variable instead of object variable
        # Supported methods of reading in dataset. 
        self.datasetMethods = ["ArcFaceCasia", "ArcFaceCasiaMedley"]
        
        # TODO: Consider class variable instead of object variable
        # Supported image folder structures
        self.imgfolderstructure = ["subjectnr/subjectnr_samplenr", "subjectnr/samplenr"]

        # TODO: Consider class variable instead of object variable
        # Supported image folder structures
        self.faceRecognitionMethods = ["ArcFace"]

        self.bh = None
        self.img_obs = []
        self.imgRead = False
        self.sample_nr = 0
        self.subject_nr = 0
        self.BaselineComputed = False
        
        self.qualmetrics = []
        self.projectpath = projectpath
        self.imgsdir = imgsdir


        self.Arcface_inited = False
        self.Arcface_detection_inited = False

    '''
    Inits ArcFace used for comparisons
    '''
    def ArcFace_init(self):
        # Set up detector and recognizer
        self.app = FaceAnalysis(providers=['CUDAExecutionProvider', 'CPUExecutionProvider'])
        self.app.prepare(ctx_id=0, det_size=(640, 640))
        
        self.handler = insightface.model_zoo.get_model('buffalo_l')
        self.handler.prepare(ctx_id=0)
        self.Arcface_inited = True

    '''
    Inits ArcFace used for detection
    '''
    def Detection_Arcface_init(self):   
        if  not self.Arcface_inited:
            self.app_det = FaceAnalysis(allowed_modules=['detection']) # enable detection model only
            self.app_det.prepare(ctx_id=0, det_size=(640, 640))
            self.Arcface_detection_inited = True
            return 0
        else: 
            self.app_det = self.app
            self.Arcface_detection_inited = True
            return 0

    '''
    Reads in a dataset for a given folderstructure
    '''
    def readInDataset(self, params: list, projectpath: str, imgsdir: str,  datasetMethod: str, debug=False) -> int:
        self.bh = BaseHandler(params[0], params[1])
        projectpath =  self.projectpath
        imgsdir = self.imgsdir
        if self.imgRead: 
            raise Exception("Already Read Images")
        
        # Validate params
        if len(params) != 2:
            return -1

        # Use inread method 
        if self.setFolderStructure == self.imgfolderstructure[0]: 
            if datasetMethod == self.datasetMethods[0]: 
                self._readInBySubNR(projectpath, imgsdir, debug, params[0], params[1])
            else: 
                print("Dataset method not supported")
        elif self.setFolderStructure == self.imgfolderstructure[1]: 
            if datasetMethod == self.datasetMethods[0]: 
                self._readInBySubnrShortened(projectpath, imgsdir, debug, params[0], params[1])
            else: 
                print("Dataset method not supported")
        else: print("Folderstructure not supported")
                
            
        if len(self.img_obs) <= 0: 
            print("Something went wrong when reading in images")
            return -1
        else:
            self.imgRead = True
            return 0 
    
    '''
    Reads images in for the folderstructure subjectnr/samplenr
    '''
    def _readInBySubnrShortened(self, projectpath: str, imgsdir: str, debug: bool, subject_nr: int, sample_nr: int):
        if not self.ArcFace_init: 
            raise Exception("ArcFace Not inited")
     
        c = 1
        for i in range (c, subject_nr+1):
            
            if i<10: subjectnr =  '000000' + str(i) 
            elif i<100: subjectnr =  '00000' + str(i) 
            else: subjectnr = '0000' +str(i)
           
            subjectdir =   subjectnr + '/'
            
            if(debug):
                print("Path" + imgsdir + subjectdir)
                print("Read in for: " + str(i) + "-th subject")

            reader = Reader()
            reader.setArcFaceVars(self.app, self.handler)
            
            self.img_obs.append([])
            for j in range(1, sample_nr+1):
                #print(j)
                fullpath = projectpath + imgsdir + subjectdir
                imgname =  '00' + str(j)

                imgob = reader.read(fullpath + imgname, imgname, True)
                self.img_obs[i-c].append(imgob)

        return 0

    '''
    Reads images in for the folderstructure subjectnr/subjectnr_samplenr
    '''
    def _readInBySubNR(self, projectpath: str, imgsdir: str, debug: bool, subject_nr: int, sample_nr: int):
        if not self.ArcFace_init: 
            raise Exception("ArcFace Not inited")
        
        # Read in images, faces and features 
        for i in range (0, subject_nr):

            
            subjectnr =  '00' + str(i) if i<10 else '0' + str(i)
            subjectdir =   subjectnr + '/'
            
            if(debug):
                print("Path" + imgsdir + subjectdir)
                print("Read in for: " + str(i) + "-th subject")

            reader = Reader()
            reader.setArcFaceVars(self.app, self.handler)
            
            self.img_obs.append([])
            for j in range(0, sample_nr):
                fullpath = projectpath + imgsdir + subjectdir
                imgname =  subjectnr + '_' + str(j)
                imgob = reader.read(fullpath + imgname, imgname, True)
                self.img_obs[i].append(imgob)

        return 0

    '''
    Compute baseline scores
    '''
    def ComputeBaselines(self, method="ArcFace", debug=False):
        if not self.imgRead: raise Exception("Images not read in yet")
        
        if method == self.faceRecognitionMethods[0]:
            status = self._ArcFaceBaseline(debug)

        if status == 0: self.BaselineComputed = True
        return status
        
    '''
    Save Baseline scores
    '''
    def SaveBaslines(self, spath=""): 
        if not self.BaselineComputed:
            print("Basline Not Computed, will not save")
            return -1

        if spath == "": 
            spath = "data/sim_scores_" + self.instaceName
        
        # Save to file
        new_array = np.array(self.sim_scores)
        print("Saving to: ", spath)
        np.save(spath, new_array)

    '''
    Use Arcface to compute Baselines
    '''
    def _ArcFaceBaseline(self, debug):
        if not self.ArcFace_init: 
            raise Exception("ArcFace Not inited")

        sim_scores = []

        for i in range(0, len(self.img_obs)):
            if debug: print("Compute sim scores for: " + str(i) + "-th subject")
            for j in range(0, len(self.img_obs[i])):
                pin = j
                for k in range(i, len(self.img_obs)): 
                    for l in range (pin, len(self.img_obs[k])): 
                        sim_scores.append([])
                        pin = len(sim_scores)
                        sim_scores[pin-1].append(self.handler.compute_sim(self.img_obs[i][j].feat, self.img_obs[k][l].feat ))
                        sim_scores[pin-1].append(self.img_obs[i][j].name + " x " + self.img_obs[k][l].name)
                    pin = 0 
        

        if len(sim_scores) != 0: 
            self.sim_scores = sim_scores
            return 0
        else: 
            print("Something went wrong while computing")
            return -1

    '''
    Computes the specified metric
    '''
    def ComputeMetric(self, metric:str,  subject_tot=-1, sample_tot = -1):
        m = QualityMetric(metric, self.app_det)
        if subject_tot==-1 and sample_tot==-1: 
            subject_tot = self.bh.subject_tot
            sample_tot = self.bh.sample_tot

        m.calculate(self.projectpath,self.imgsdir, subject_tot, sample_tot, self.setFolderStructure ,debug=True)

        self.qualmetrics.append(m)

        return 0

    '''
    Returns the specifed metric if it is read in
    '''
    def _findMetric(self, metric):
        for i in range(len(self.qualmetrics)):
            if self.qualmetrics[i].metric == metric:
                return self.qualmetrics[i]
        return None

    '''
    Save the specified metric to file
    '''
    def SaveMetric(self, metric:str, metricname="", normalized=False): 
        if metricname == "": 
            metricname = "data/" + self.instaceName + "_" + metric
            if normalized: metricname = metricname + "_norm"
        
        qual = self._findMetric(metric)

        if qual != None:
            np.save(metricname, np.array(qual.getQualityValues()))
            return 0
        
        
        return -1
    
    '''
    Read in array from csv file
    '''
    def _readAsCsv(self, name):
        name = name + ".csv"
        with open(name, newline='') as csvfile:
            data_s = list(csv.reader(csvfile))
        data =  [ [0]*len(data_s[0]) for i in range(len(data_s))]

        # Convert datatype
        for i in range(0, len(data_s)): 
            for j in range(0, len(data_s[0])):
                data[i][j] = float(data_s[i][ j])
        return data
    
    '''
    Read in array from npy file
    '''
    def _readNumpyArr(self, name):
        name = name +".npy"
        data = np.load(name)
        return data

    '''
    Reads in a metric from file
    '''
    def ReadMetric(self, metric, metric_name="", normalized=False):
        if not self.Arcface_detection_inited:
            raise Exception("Arcface Detection not initiated")

        # Construct file name
        name = self.projectpath
        if metric_name == "": 
            name = name + "data/" + self.instaceName
            name = name + "_" + metric
            if(normalized): name = name + "_norm"
        else:
            name = name + metric_name

        # Format 
        # TODO: This is a bit hacky, consider changing
        if (metric != "brisque"):
            met = self._readNumpyArr(name)
        else: 
            met = self._readAsCsv(name)

        # Add to inited metrics
        m = QualityMetric(metric, self.app_det, quals=met)
        self.qualmetrics.append(m)
        
        return m
    
    '''
    Plot the specified metric according to the specified method
    '''
    def PlotMetric(self, method, metric, metric_name ="", simscore_names ="" , subnr = -1, sampnr =-1): 
       
        # Get quality values
        qualVal = self._findMetric(metric)
        if str(type(qualVal)) == "<class 'NoneType'>": qualVal = self.ReadMetric(metric, metric_name).getQualityValues()
        else: qualVal = np.array(qualVal.getQualityValues())

        if str(type(qualVal)) == "<class 'NoneType'>": 
            print("Can not read metric")
            return -1
        
        # Init basehandler
        if self.bh == None:
            bh = BaseHandler(subnr, sampnr)
        else: 
            bh = self.bh

        # Plot metric
        if type(qualVal) != "<class 'NoneType'>":
            if method == "DET": 
                sims = self._findBaseline(simscores_name=simscore_names)
                return self._PlotDET(sims, qualVal, bh,)
            elif method  == "EDC": 
                sims = self._findBaseline(simscores_name=simscore_names)
                return self._PlotEDC(sims, qualVal, bh, metric)
            else: 
                print("Plot method does not exist")
                return -1

        
    '''
    Returns the baseline scores 
    '''
    def _findBaseline(self, simscores_name="") -> list: 
        if self.BaselineComputed and simscores_name=="": 
            sim = self.sim_scores
        else: 
            if simscores_name == "": name = "data/sim_scores_" + self.instaceName
            else: name = simscores_name 
            name = name + ".npy"
            
            sim = np.load(name)

        return sim

    '''
    Plots the DET for a metric at all quality levels
    '''
    def _PlotDET(self, sims, quals, bh=None): 

        qe = ValueHandling()
        if bh == None: 
            bh = self.bh
            print("Use inited BaseHandler")

        for i in range(1,6): 
            g_p,i_p = qe.removeLowQualValues(sims, quals, bh, i) 

            if  len(i_p) != 0  and len(g_p) != 0: 
                #print(i, ": Have gorund: " ,max(i_p) > min(i_p)) 
                qe.plotDET(g_p, i_p, colour[i])
            else: 
                print(i, ": Empty: ", len(g_p), " || ", len(i_p))
        
        qe.showDET()
        return

    '''
    Plots the EDC for a metric
    '''
    def _PlotEDC(self, sim_scores, qual_scores, bh: BaseHandler, metric): 
        low = 1
        high = 5  
        
        edc = EDC(lowerbound=low,higherbound=high, fixed_fmnr_threshold=0.3, metricname=metric, style="-")
        edc.setup()

        qu = ValueHandling(True)

        gens, quals = qu.getAllGenuine(bh, sim_scores, qual_scores)
        gens = np.array(gens)
        quals = np.array(quals)
        
        edc.plotEDC(gens, quals)
        edc.showEDC()
     
    '''
    Plots the DET curve for the baselines
    '''
    def PlotDETCurve(self, subnr=-1, sampnr=-1):
        det= DET(biometric_evaluation_type = 'algorithm', extend_plot=True)
        det.create_figure()

        if self.BaselineComputed:
            sims = np.array(self.sim_scores)
        else: 
            sims= np.load("data/sim_scores_" + self.instaceName + ".npy")

        if self.bh == None: 
            if sampnr <0 or subnr <0: raise Exception("Provided sample number or subject number is not valid")    
            bh = BaseHandler(subnr, sampnr)
            
        else: bh = self.bh
        qa = ValueHandling(True)
        gens = qa.getAllGenuine(bh, sims)
        imps = qa.getAllImposters(bh, sims)

        #print("Gens: ", len(gens))
        #print("Imps: ", len(imps))

        det.plot(np.array(gens), np.array(imps), "Kitty")
        det.show()


'''
High level representation of a qualit metric
'''
class QualityMetric:
    supported_metrics = ["sharpness", "focus", "brisque"]
    supported_folder_structure = ["subjectnr/subjectnr_samplenr", "subjectnr/samplenr"]
    def __init__(self, metric:str, app, quals=None) -> None:

        if metric == QualityMetric.supported_metrics[0]: self.calcRawQual = self.calcSharpness
        elif metric == self.supported_metrics[1]: self.calcRawQual = self.calcFocus
        elif metric == self.supported_metrics[2]: self.calcRawQual = self.calcBrisque
        else: raise Exception(metric + " is not supported")

        self.metric = metric
        self.app = app
        self.qualValues = []
        self.qualValuesRaw = []
        
        if str(type(quals)) != "<class 'NoneType'>": 
            self.qualValues = quals
        else: 
            self.qualValues = []

    '''
    Calculates the brisqu value for a given image. Not implemented; it is meerly here for compatabillity. 
    See seperate matlab script for brisque calculation
    '''
    def calcBrisque(self, image):
        raise Exception("Not implemented, use matlab script instead")

    '''
    Calculates the sharpness metric for a gien image
    '''
    def calcSharpness(self, image):
            image_handler = Image_Preparator(self.app)
            q = QualityAssesmenet()
            square= image_handler.segment_square(image)
            lum_s = image_handler.luimanence(square)
            s = q.Sharpness(lum_s)   
            return s

    '''
    Calculates the sharpness metric for a gien image
    '''
    def calcFocus(self, image):
        # Get segments
        image_handler = Image_Preparator(self.app)
        q = QualityAssesmenet()
        crop = image_handler.segment(image)
        lum = image_handler.luimanence(crop)
        f = q.de_focus(lum)
        return f


    '''
    Mehtod called to calculate the set metric
    '''
    def calculate(self, projectpath, imgsdir, subnr, samplenr, folderstructure="subjectnr/subjectnr_samplenr", debug = False):
        
        # Init reader
        reader = Reader()
        reader.setArcFaceVars(self.app, None)
        qualValues  = []


        if debug: print("\n--Calculating Quality Scores --\n")

        # Calculate based on the given folder structure
        if(folderstructure == self.supported_folder_structure[0]):
            
            # For each subject
            for i in range (0, subnr):
                subjectnr =  '00' + str(i) if i<10 else '0' + str(i)
                subjectdir =   subjectnr + '/'
                
                
                if debug: print("Read in for: " + str(i) + "-th subject")
                
                
                qualValues.append([])


                for j in range(0, samplenr):
                    # Pathing
                    fullpath = projectpath + imgsdir + subjectdir
                    imgname =  subjectnr + '_' + str(j)      

                    # Get Image    
                    image = reader.read(fullpath +  imgname,imgname, False)

                    # Get quality Value
                    q = self.calcRawQual(image.img)

                    # Add value
                    qualValues[i].append(q)
            
            # Set values
            self.qualValuesRaw = qualValues
            self.qualValues = self.quality_normalize(qualValues)

        elif folderstructure == self.supported_folder_structure[1]: 
           
            # For each subject
            for i in range (1, subnr+1):
                
                if i<10: subjectnr =  '000000' + str(i) 
                elif i<100: subjectnr =  '00000' + str(i) 
                else: subjectnr = '0000' +str(i)
            
                subjectdir =   subjectnr + '/'
                
                if(debug):
                    print("Path" + imgsdir + subjectdir)
                    print("Read in for: " + str(i) + "-th subject")

                qualValues.append([])
                for j in range(1, samplenr+1):
                    fullpath = projectpath + imgsdir + subjectdir
                    imgname =  '00' + str(j)
                    image = reader.read(fullpath +  imgname,imgname, False)

                    # Get quality Value
                    q = self.calcRawQual(image.img)
                    
                    # Add value
                    qualValues[i-1].append(q)
            
            # Set values
            self.qualValuesRaw = qualValues
            self.qualValues = self.quality_normalize(qualValues)
        return
    
    '''
    Return quality values 
    '''
    def getQualityValues(self):
        return self.qualValues

    '''
    Bin quality vales fro [0,100] -> [1,5]
    '''
    def quality_normalize(self, m):
        f_norm = m

        for i in range(len(f_norm)):
            for j in range(len(f_norm[0])): 
                if f_norm[i][j] < 20: f_norm[i][j] = 1
                if f_norm[i][j] < 40 and f_norm[i][j] >= 20: f_norm[i][j] = 2
                if f_norm[i][j] < 60 and f_norm[i][j] >= 40: f_norm[i][j] = 3
                if f_norm[i][j] < 80 and f_norm[i][j] >= 60: f_norm[i][j] = 4
                if f_norm[i][j] >= 80: f_norm[i][j] = 5
        
        return f_norm


'''
Handling similarity and quality values
'''
class ValueHandling:
    def __init__(self, dropDET=False) -> None:
        if not dropDET:
            self.d =  DET(biometric_evaluation_type = 'algorithm', extend_plot=True)
            self.d.create_figure()
        pass
    
    '''
    Return all imposters from the sim scores. 
    '''
    def getAllImposters(self, bh: BaseHandler, sim_scores ):
            imposters = []
            for i in range(0, bh.subject_tot):   
                for j in range(0, bh.sample_tot):
                    pin = j
                    for k in range(i+1, bh.subject_tot): 
                        for l in range (0, bh.sample_tot): 
                            if (i!=k):
                                imposters.append(float(sim_scores[bh.get( i, j, k, l)][0]))
                        pin = 0 
                    
            return imposters

    '''
    Return all genuine scores from the sim scores. 
    '''
    def getAllGenuine(self, bh: BaseHandler, sim_scores, qual_scores=None):
        if str(type(qual_scores)) != "<class 'NoneType'>": 
            calc_qual = True
            quals = []
        else: calc_qual = False

        genuine = []
        for i in range(0, bh.subject_tot):   
            for j in range(0, bh.sample_tot):
                pin = j
                for k in range(i, bh.subject_tot): 
                    for l in range (pin+1, bh.sample_tot): 
                        if (i==k):
                            genuine.append(float(sim_scores[bh.get( i, j, k, l)][0]))
                            
                            if (calc_qual): 
                                quals.append([])
                                last = len(quals)-1
                                quals[last].append(qual_scores[i][j])
                                quals[last].append(qual_scores[k][l])
                    pin = 0 
                
        if calc_qual: return genuine, quals
        else: return genuine

    '''
    Combine quality values
    '''
    def H(self, q1: int, q2: int): 
        return int((q1+q2)/2)

    '''
    Evaulate if discard of keep
    '''
    def eval_quality(self, q1: int, q2:int, k: int) -> bool: 
        return (q1==k and q2>=k)
        #return self.H(q1, q2) == k

    '''
    Remove low values
    '''
    def removeLowQualValues(self, sim_scores: list, quality_scores: list, bh: BaseHandler, ql:int) -> list: 
        #g_s, i_s = prepare(getAllGenuine(bh, sim_scores), getAllImposters(bh, sim_scores))
        g_s_new = []
        i_s_new = []

        # For all subject combinations 
        for i in range(bh.subject_tot):
            for j in range(bh.sample_tot): 
                for k in range(i, bh.subject_tot): 
                    for l in range(bh.sample_tot): 

                        #Save if quality scores exeeds requierments
                        q1 = quality_scores[i][j]
                        q2 = quality_scores[k][l]
                        s = sim_scores[bh.get(i,j,k,l)]

                        #print("S score: " + s[1], "|| ", i, " ", j, " x ", k, " ", l)
                        if (self.eval_quality(q1, q2, ql)):
                            if(i==k): 
                                if j<l: g_s_new.append(float(s[0]))
                            else: 
                                i_s_new.append(float(s[0]))

        return g_s_new, i_s_new

    '''
    Plot the DET 
    '''
    def plotDET(self, genuine: list, imposters:list, col: str):
        self.d.plot(np.array(genuine), np.array(imposters), col )

    '''
    show the DET plot
    '''
    def showDET(self):
        self.d.show()

    
'''
Low level quality operations
'''
class QualityAssesmenet:
    def __init__(self) -> None:
        self.a = 0.007875693622098313
        self.b =  0.0032

        self.c =  7.827441428389922
        self.d =  2.2 
        pass

    '''
    Calculate Focus metric
    '''
    def de_focus(self, luiminance_map):
        Bs = self.mean_filter(luiminance_map)
        Ds = abs(Bs - luiminance_map)
     
        dim = Ds.shape

        dims = dim[0]*dim[1]

        Blurloss = (sum(sum(Ds)))/(dims)

        return (self.SIGMOID(Blurloss, self.a, self.b))*100
        #return Blurloss

    '''
    Calculate mean_filter 
    '''
    def mean_filter(self, arr, filtersize=3):
        w = np.full((filtersize, filtersize), 1.0/(filtersize*filtersize))
        return scipy.ndimage.filters.convolve(arr, w)
    


    '''
    Calculate Sharpness Metric
    '''
    def Sharpness(self, luiminance_map): 
        
        n = len(luiminance_map)
        Hm = luiminance_map
        
        # Find mean of H
        mean = np.mean(Hm)
        Gm = Hm
        
        # Subtract mean from every normalized pixel values
        for i in range(len(luiminance_map)): 
            for j in range(len(luiminance_map[0])): 
                if Gm[i][j] != 0: Gm[i][j] = Gm[i][j] - mean
        
        # Compute the covariance matrix
        np_G = np.array(Gm)
        np_G_t = np.transpose(np_G)
        
        Ss  = (1/(n-1))*np.matmul(np_G, np_G_t)
        
        # Find N eigenvalues of D
        eigs = np.linalg.eig(Ss)[0]
  
        # Compute the trace sum of eigenvalues 
        sum = 0
        for i in range(len(eigs)):
            sum = sum + float(eigs[i])

        return 100*(self.SIGMOID(sum,self.c,self.d ))
        #return sum

    '''
    Calculate SIGMOID function
    '''
    def SIGMOID(self, x, x0, w): 
        return float(1/(1 + np.exp((x0-x)/w)))


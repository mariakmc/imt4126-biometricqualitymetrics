from urllib.request import BaseHandler
from qualityEvaluation import QualityEvaluator, QualityMetric
from insightface.app import FaceAnalysis
from insightface.data import get_image as ins_get_image
from EDC import EDC
import numpy as np
from qualityEvaluation import ValueHandling as QualityTemp
from utils.baseScore import BaseHandler
import matplotlib.pyplot as mpl


# Init vars
#params = [51, 5]
params = [106, 5]

#####################################################
## STEP 0: CHANGE projectpath TO LOCAL PROJECT PATH ##
#####################################################

projectpath = 'C:/Users/maria/Desktop/Skole/Semester2 - Master/Biometrics/TermPaper/imt4126-biometricqualitymetrics/'

imgsdir = 'img/CasiaV2/CasiaSelection/'



###############################
## STEP 1: COMPUTE BASELINES ##
###############################

# Init Evaulator
print("--* Initing Evaulator")
# "subjectnr/samplenr" = 000001/001.jpg
QualEval = QualityEvaluator("ArcFaceCasiaV2", projectpath, imgsdir, "subjectnr/samplenr")


print("-- * Initing Arcface")
#QualEval.ArcFace_init()

print("--* Reading in Dataset")
# Read in dataset (with debug)
#QualEval.readInDataset(params, projectpath, imgsdir, "ArcFaceCasia", True ) 

#Compute baseline
print("--* Computing Baselines")
#QualEval.ComputeBaselines()

print("--* Saving Baselines")
#QualEval.SaveBaslines()




######################################
## STEP 2: COMPUTE METRICS          ##
######################################

## NB! REMEMBER TO UNCOMMENT the QualEval initiation ON LINE 35

# Compute metrics 
print("--* Detecting Init")
#QualEval.Detection_Arcface_init()

print("--* Computing metrics")
#QualEval.ComputeMetric("sharpness", params[0], params[1])
#QualEval.ComputeMetric("focus", params[0], params[1])

print("--* Save metric")
#QualEval.SaveMetric("sharpness")
#QualEval.SaveMetric("focus")





##########################
## STEP 3: PLOT METRICS ##
##########################

## NB! REMEMBER TO UNCOMMOENT the QualEval initiation ON LINE 35 and QualEval.Detection_Arcface_init()  ON LINE 63

print("--* Plotting Metrics")
#QualEval.PlotMetric("EDC", "focus",subnr=params[0], sampnr=params[1])
#QualEval.PlotMetric("EDC", "sharpness",subnr=params[0], sampnr=params[1])
#QualEval.PlotMetric("EDC", "brisque",subnr=params[0], sampnr=params[1], metric_name="data/brisque/quality_values_aug")
print("DET; Focus")
#QualEval.PlotMetric("DET", "focus",subnr=params[0], sampnr=params[1])
print("DET; Sharpness")
#QualEval.PlotMetric("DET", "sharpness",subnr=params[0], sampnr=params[1])
print("DET; Brisque")
#QualEval.PlotMetric("DET", "brisque",subnr=params[0], sampnr=params[1], metric_name="data/brisque/quality_values_aug")

#QualEval.PlotDETCurve(params[0],params[1])


